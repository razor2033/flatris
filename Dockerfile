#Use container NODE.js
FROM node

#Create and move pr. dir
RUN mkdir  /skilbox
#WORKDIR /skilbox

#Copy local pr depend.
COPY ./flatris/package.json /skilbox
WORKDIR /skilbox
RUN yarn install
COPY ./flatris /skilbox

#start unit.test
RUN yarn install
#key -u fix failed yarn test stage
RUN yarn test -u
RUN yarn build


#start app
CMD yarn start

# open node port
EXPOSE 3000

